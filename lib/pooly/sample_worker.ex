defmodule Pooly.SampleWorker do
  use GenServer

  def start_link(args) do
    GenServer.start_link(__MODULE__, :ok, args)
  end

  def stop(pid) do
    GenServer.call(pid, :stop)
  end

  def init(_) do
    {:ok, self()}
  end

  def handle_call(:stop, _from, state) do
    {:stop, :normal, :ok, state}
  end
end
