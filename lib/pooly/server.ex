defmodule Pooly.Server do
  use GenServer

  @name __MODULE__

  defmodule State do
    defstruct sup: nil, size: nil, worker_sup: nil, workers: nil, monitors: nil
  end

  #######
  # API #
  #######

  def start_link([sup, pool_config]) do
    GenServer.start_link(@name, [sup, pool_config], name: @name)
  end

  def show_state() do
    GenServer.call(@name, :show_state)
  end

  def checkout() do
    GenServer.call(@name, :checkout)
  end

  def status do
    GenServer.call(@name, :status)
  end

  def checkin(worker_pid) do
    GenServer.cast(@name, {:checkin, worker_pid})
  end

  #############
  # Callbacks #
  #############

  def init([sup, pool_config]) when is_pid(sup) do
    monitors = :ets.new(:monitors, [:private])
    init(pool_config, %State{sup: sup, monitors: monitors})
  end

  def init([{:size, size} | rest], state) do
    init(rest, %{state | size: size})
  end

  def init([_ | rest], state) do
    init(rest, state)
  end

  def init([], state) do
    send(self(), :start_worker_supervisor)
    {:ok, state}
  end

  ### call ###

  def handle_call(:show_state, _from, state) do
    {:reply, state, state}
  end

  def handle_call(:checkout, {from_pid, _ref}, %{workers: workers, monitors: monitors} = state) do
    case workers do
      [worker | rest] ->
        ref = Process.monitor(from_pid)
        true = :ets.insert(monitors, {worker, ref})
        {:reply, worker, %{state | workers: rest}}

      [] ->
        {:reply, :noproc, state}
    end
  end

  def handle_call(:status, _from, %{workers: workers, monitors: monitors} = state) do
    {:reply, {length(workers), :ets.info(monitors, :size)}, state}
  end

  ### cast ###

  def handle_cast({:checkin, worker}, %{workers: workers, monitors: monitors} = state) do
    case :ets.lookup(monitors, worker) do
      [{pid, ref}] ->
        true = Process.demonitor(ref)
        true = :ets.delete(monitors, pid)
        {:noreply, %{state | workers: [pid | workers]}}

      [] ->
        {:noreply, state}
    end
  end

  ### info ###

  def handle_info(:start_worker_supervisor, state = %{sup: sup, size: size}) do
    worker_sup_spec = {Pooly.WorkerSupervisor, []}
    {:ok, worker_sup} = Supervisor.start_child(sup, worker_sup_spec)
    worker_spec = {Pooly.SampleWorker, []}
    workers = prepopulate(size, worker_sup, worker_spec)

    {:noreply, %{state | worker_sup: worker_sup, workers: workers}}
  end

  ###########
  # Helpers #
  ###########

  defp prepopulate(size, worker_sup, worker_spec) do
    prepopulate(size, worker_sup, worker_spec, [])
  end

  defp prepopulate(size, _worker_sup, _worker_spec, workers) when size < 1 do
    workers
  end

  defp prepopulate(size, worker_sup, worker_spec, workers) do
    prepopulate(
      size - 1,
      worker_sup,
      worker_spec,
      [new_worker(worker_sup, worker_spec) | workers]
    )
  end

  defp new_worker(worker_sup, worker_spec) do
    {:ok, worker} = DynamicSupervisor.start_child(worker_sup, worker_spec)
    worker
  end
end
