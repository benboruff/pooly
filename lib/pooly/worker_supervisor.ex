defmodule Pooly.WorkerSupervisor do
  use DynamicSupervisor, type: :supervisor, restart: :temporary

  #######
  # API #
  #######

  def start_link(_) do
    DynamicSupervisor.start_link(__MODULE__, :ok, name: __MODULE__)
  end

  #############
  # Callbacks #
  #############

  def init(_init_arg) do
    DynamicSupervisor.init(
      strategy: :one_for_one,
      max_restarts: 5,
      max_seconds: 5
    )
  end

  ###########
  # Helpers #
  ###########
end
